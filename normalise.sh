#!  /bin/bash
#
# Convert punctuation to spaces, and upper case letters to lower case.
# This script should only be used on relatively small input!

cat - | tr --complement '[:alnum:][:blank:]' ' ' | tr '[:upper:]' '[:lower:]'

# vim: ft=sh ts=4 sw=4 et
