\documentclass[a4paper]{article}

\title{Cluster and Cloud Computing -- Assignment 1}
\author{Matthew Signorini (350673)}

\usepackage{graphicx}

\begin{document}

\maketitle


\section{Overview}

The objective of this project was to develop an application to search through a large volume of
text for a set of keywords, counting the number of times each keyword occurs. The application is
to make use of the Edward HPC facility, allowing the application to scale to large data files by
utilising many processors simultaneously. The output will also include the time that search took,
in terms of wall time from when the search was started to when the results were output. We will
use these time values in this report to analyse how the application's performance varies when more
processors and/or larger data files are used.


\section{Invoking the search system}

The search application consists of four shell script files, and two C programs, which are invoked
by the shell scripts. There is a makefile to build the C programs, run \verb|make all| from the top
level makefile to build the programs and copy them to the \verb|bin/| subdirectory of your git
workspace.

The properties of the search run are set by modifying values in \verb|const.sh|, in particular the
file to search through, \verb|INPUT_FILE|, and the file containing the keywords to search for,
\verb|QUERY_FILE|, as well as what size chunks to break the input file into, and a spool directory
where the application will store the keyword counts for individual chunks of the input file.

Once all these values are set, the application is executed by submitting \verb|parallel-search.sh|
as a PBS job using the \verb|qsub| command on Edward. The number of nodes and processors per node
are set using the PBS macros at the top of \verb|parallel-search.sh|. The search results and the
elapsed wall time are written onto stdout, collected by Edward into a file for later viewing.
Debugging information is also printed onto stderr.


\section{How it was implemented}

This application takes a MapReduce style approach to the problem. The top level script,
\verb|parallel-search.sh|, determines what nodes have been assigned to it by PBS, then starts
processes on those nodes to search through a chunk of the file. Each chunk is an integer number
of lines, extracted from the input file using the \verb|head| and \verb|tail| Unix commands.
\verb|task.sh| is the script responsible for searching through a chunk of the input, then storing
the keyword counts for that chunk in a spool directory. It invokes the \verb|search| C program,
and redirects it's output to a file created by the \verb|mktemp| command, to ensure unique file
names in the spool directory. Once all of the chunks have been processed,
\verb|parallel-search.sh| uses the \verb|aggregate| C program to merge all the spooled search
fragments. Thanks are due to Dirk Van Der Knijff for providing his task farm demo, located under
\verb|/data/user1/dirk/sinnott/taskfarm| on Edward, from which this application was adapted.

The MapReduce approach has a definite advantage in scalability: Utilising more processors is
simply a matter of increasing the number of nodes requested in the PBS macros. No modifications to
the shell script or C programs are necessary, as \verb|parallel-search.sh| will make use of the
extra nodes automatically.

For the purposes of this project, a word is defined as a contiguous sequence of one or more
alphanumeric characters. Punctuation characters, including hyphens, are treated as white space,
and the application assumes that the complete word exists on a single line, as opposed to being
split over two lines, with a hyphen between the two parts. Hyphenated words are treated as two
separate words.


\section{Performance Analysis}

Following are three graphs of the wall time taken to search through three different sample files.
For each file, three different search runs were performed, using a single core on one node, 8 cores
on one node, and using 2 nodes, with 4 cores per node (making 8 cores in total).

Times were measured within the script, using the Unix \verb|date| program to determine the number of
seconds between starting and finishing.
\begin{figure}
    \includegraphics[width=12cm,height=8cm]{./small.png}
    \caption{Wall time in seconds taken to search through the small file}
\end{figure}

\begin{figure}
    \includegraphics[width=12cm,height=8cm]{./medium.png}
    \caption{Wall time in seconds to search through the medium file|}
\end{figure}

Searching \verb|CCCdata-large.txt| using 1 node and 1 core took more than 1 hour and 30 minutes,
which was the time allocated to the job by PBS, causing the job to be aborted. The time for this
combination was recorded as 5400 seconds.
\begin{figure}
    \includegraphics[width=12cm,height=8cm]{./large.png}
    \caption{Wall time taken to search through the large file}
\end{figure}

In all cases, a substantial improvement in performance was observed when more processors were used.
For searching \verb|CCCdata-small.txt| the improvement was a factor of 4 to 5, whereas for the larger
medium file the improvement was closer to a factor of 7. The results for the large file are skewed by
the fact that the single-processor run was aborted slightly early.


\section{Conclusion}

In this project, we have developed an application to make use of an arbitrary number of nodes of the
Edward HPC cluster to search for keywords in a text file. Measuring the time taken for the job to
complete has shown that making use of more processors has a significant effect in reducing the time
taken for the search.


\end{document}

% vim: set ft=tex ts=4 sw=4 et
