# top level makefile. This is a bit redundant, but at least it allows the
# project to be built and set up in a single command.

all:
	cd src && $(MAKE) all
	mkdir -p bin
	cp src/search src/aggregate bin/

.PHONY:		all
