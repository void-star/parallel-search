#!  /bin/bash
#
# Script to search for keywords in a section of a given file and output a
# summary of the number of times each keyword occurs. Output is stored in
# a file for subsequent merging with output for other chunks.
#
# This script has a single parameter, the offset in lines of the start of
# the chunk that we are to process.


. /home/mps/comp90024/parallel-search/const.sh

cd ${TASK_PWD}
FRAGMENT_FILE=`mktemp --tmpdir=${FRAGMENTS_DIR} XXXXXXXX`
QUERY=`cat ${QUERY_FILE} | ./normalise.sh`
head --lines=$(($1 + ${LINES_PER_CHUNK})) ${INPUT_FILE} | \
    tail --lines=${LINES_PER_CHUNK} | ./bin/search ${QUERY} > ${FRAGMENT_FILE}

echo ${HOSTNAME} > `mktemp --tmpdir=${NODES_DIR} XXXXXX`

# vim: ft=sh ts=4 sw=4 et
