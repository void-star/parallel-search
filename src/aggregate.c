/**
 *  Program to aggregate the output from search.c, ie. merge multiple
 *  search runs. The search traces to be merged are to be fed into this
 *  program's stdin stream; duplicate words will be detected, and their
 *  counts summed.
 *
 *  Author: Matthew Signorini (mps)
 */


// this macro must be defined in order for getline to be included from stdin.
#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>

#include "utils.h"


PRIVATE void merge_keyword (const char *keyword, int partial_count);
PRIVATE void print_aggregate (void);


typedef struct keyword
{
    const char *keyword;
    int counter;
    struct keyword *next;
}
keyword_t;


// global variable for the linked list of unique keywords.
keyword_t *keyword_list = NULL;


/**
 *  Entry point. This program doesn't take any command line arguments.
 *  Lines of search output go into stdin, and once EOF is reached, merged
 *  search results come out of stdout. Lol, that sounds like a euphemism
 *  for something completely different!
 */
    PUBLIC int
main (argc, argv)
    int argc;           // argc and argv aren't really used here, given
    char **argv;        // that we don't take any arguments.
{
    char *buffer = NULL;
    size_t buffer_length = 0;
    char *word;
    int partial_count;

    // read from stdin, one line at a time, then parse the line for the
    // keyword, and partial count.
    while (getline (&buffer, &buffer_length, stdin) != -1)
    {
        if (sscanf (buffer, "%ms #%d\n", &word, &partial_count) != 2)
        {
            // TODO: print error message.
            break;
        }

        merge_keyword (word, partial_count);
    }

    print_aggregate ();

    return 0;
}

/**
 *  Check if the given word is present in the list of 
 */
    PRIVATE void
merge_keyword (keyword, partial_count)
    const char *keyword;    // keyword to check for in the list.
    int partial_count;      // count of occurrences in a chunk of the input.
{
    keyword_t **current_word = NULL;
    keyword_t *new_keyword;

    for (current_word = &keyword_list; *current_word != NULL; 
      current_word = & ((*current_word)->next))
    {
        if (strcmp ((*current_word)->keyword, keyword) == 0)
        {
            (*current_word)->counter += partial_count;
            return;
        }
    }

    // if we reach this point, then the keyword does not exist in the list. Add it.
    fprintf (stderr, "Adding new keyword \"%s\" to the list.\n", keyword);
    new_keyword = (keyword_t *) safe_malloc (sizeof (keyword_t));
    new_keyword->keyword = keyword;
    new_keyword->counter = partial_count;
    new_keyword->next = keyword_list;
    keyword_list = new_keyword;
}

/**
 *  Print the cumulative total occurrences of all the keywords.
 */
    PRIVATE void
print_aggregate (void)
{
    keyword_t **current_word = NULL;
    int nr_words = 0;

    for (current_word = &keyword_list; *current_word != NULL;
      current_word = & ((*current_word)->next))
    {
        printf ("%s #%d\n", (*current_word)->keyword, (*current_word)->counter);
        nr_words += 1;
    }

    fprintf (stderr, "Printed cumulative counts for %d keywords\n", nr_words);
}

// vim: ft=c ts=4 sw=4 et
