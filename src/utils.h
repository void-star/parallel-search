/**
 *  Defines safe wrappers to malloc and realloc, as well as defining a
 *  bool type.
 *
 *  Author: Matthew Signorini (mps)
 */

#ifndef UTILS_H
#define UTILS_H


typedef int bool;

#define true        (1 != 0)
#define false       (1 == 0)


// limit the scope of a function to within a file.
#define PRIVATE     static
#define PUBLIC


// wrappers for malloc and realloc, that check for null return values.
extern void * safe_malloc (size_t size);
extern void * safe_realloc (void *buffer, size_t new_size);


#endif // UTILS_H

// vim: ft=c ts=4 sw=4 et
