/**
 *  Implements safe wrappers to malloc and realloc that do not return null.
 *
 *  Author: Matthew Signorini (mps)
 */

#include <stdlib.h>
#include <err.h>
#include <errno.h>

#include "utils.h"

/**
 *  Wrapper for C library malloc() that catches null return values.
 */
    PUBLIC void *
safe_malloc (size)
    size_t size;        // number of bytes to allocate.
{
    void *allocated = malloc (size);

    if (allocated == NULL)
        err (errno, "malloc: could not allocate %zd bytes", size);

    return allocated;
}

/**
 *  Similar wrapper for realloc.
 */
    PUBLIC void *
safe_realloc (buffer, new_size)
    void *buffer;       // malloc()'ed buffer to resize.
    size_t new_size;    // new size for the buffer.
{
    void *new_buffer = realloc (buffer, new_size);

    if (new_buffer == NULL)
        err (errno, "realloc: unable to resize buffer to %zd bytes", new_size);

    return new_buffer;
}


// vim: ft=c ts=4 sw=4 et
