/**
 *  Simple program to count the number of occurences of a set of keywords
 *  in a stream of text. The keywords are given as command line parameters,
 *  and the text to search through is read from stdin. This program does
 *  not take any options.
 *
 *  Author: Matthew Signorini (mps)
 */


#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <err.h>

#include "utils.h"


PRIVATE void init_globals (int argc, char **argv);
PRIVATE void search_for_keywords (void);
PRIVATE void print_histogram (void);
PRIVATE void increment_matching_counter (const char *sample);
PRIVATE char * get_word_from_stdin (void);
PRIVATE bool is_white_space (char sample);
PRIVATE bool more_to_read (void);
PRIVATE char normalise_character (char input);

PRIVATE void print_help (void);


// global variables to hold references to the keywords being searched for,
// and the number of times we have found each corresponding keyword in the
// text.
PRIVATE char **keywords;
PRIVATE int *counts;
PRIVATE int nr_keywords;


/**
 *  Main entry point. argc should be the number of keywords being searched
 *  for plus one; argv contains the program name, then the list of keywords.
 *
 *  TODO: eliminate case and punctuation from argv list?
 */
    PUBLIC int
main (argc, argv)
    int argc;
    char **argv;
{
    // if there are no keywords, tell the user how to use the program.
    if (argc == 1)
    {
        print_help ();
        exit (1);
    }

    init_globals (argc, argv);
    search_for_keywords ();
    print_histogram ();

    return 0;
}

/**
 *  Initialise the global variables containing the count, and keywords list.
 */
    PRIVATE void
init_globals (argc, argv)
    int argc;           // argc and argv, as passed to main.
    char **argv;
{
    counts = (int *) safe_malloc ((argc - 1) * sizeof (int));
    nr_keywords = argc - 1;
    keywords = & (argv [1]);

    for (int i = 0; i < nr_keywords; i ++)
        counts [i] = 0;
}

/**
 *  For each word from stdin, check if it matches one of the keywords, and
 *  if it does, increment the corresponding counter.
 */
    PRIVATE void
search_for_keywords (void)
{
    char *word;

    while ((word = get_word_from_stdin ()) != NULL)
    {
        increment_matching_counter (word);
        free (word);
    }
}

/**
 *  Print out each keyword and the number of times it occurred. Keywords
 *  that did not appear in the search text will not be included in the
 *  histogram.
 */
    PRIVATE void
print_histogram (void)
{
    for (int i = 0; i < nr_keywords; i ++)
    {
        if (counts [i] == 0)
            continue;

        printf ("%s #%d\n", keywords [i], counts [i]);
    }
}

/**
 *  Given a word that was read from the search text, check to see if it
 *  matches any of our list of keywords. If we do find a match, increment
 *  the hit counter corresponding to the keyword.
 */
    PRIVATE void
increment_matching_counter (sample)
    const char *sample; // word to check for a match.
{
    for (int i = 0; i < nr_keywords; i ++)
    {
        if (strcmp (sample, keywords [i]) == 0)
        {
            counts [i] += 1;
            break;
        }
    }
}

/**
 *  Reads the next "word" (consisting of a contiguous stretch of
 *  alphanumeric characters) from stdin. Any leading non-alphanumeric
 *  characters are dropped. The return value is a pointer to a character
 *  array allocated with malloc(); it is up to the caller to make sure
 *  that the buffer is later free()'ed.
 */
    PRIVATE char *
get_word_from_stdin (void)
{
    char this_char;
    char *buffer = NULL;
    int index = 0;

    // skip over any leading blanks.
    do
    {
        this_char = (char) getchar ();
    }
    while (is_white_space (this_char) && more_to_read ());

    // the skipping will have bailed on the first non blank char. Add it
    // the buffer and record all further chars until we hit a blank char.
    while ((is_white_space (this_char) == false) && more_to_read ())
    {
        buffer = (char *) safe_realloc (buffer, index + 1);
        buffer [index] = normalise_character (this_char);
        this_char = (char) getchar ();
        index ++;
    }

    return buffer;
}

/**
 *  Check if a character is valid. This program will ignore punctuation
 *  characters, so this function will treat punctuation as if they are
 *  spaces. This function will return true if the given character does not
 *  occurr in the set of upper and lower case letters and digits.
 */
    PRIVATE bool
is_white_space (sample)
    char sample;        // character to test.
{
    if (isalnum (sample) != 0)
        return false;

    return true;
}

/**
 *  Checks to see if there is any more data available on stdin. Return
 *  value is true if there is more data available, false otherwise.
 */
    PRIVATE bool
more_to_read (void)
{
    if (feof (stdin) != 0)
        return false;

    return true;
}

/**
 *  Normalises a single character; that is to say, this function turns
 *  upper case chars into lower case, and turns a non-alphanumeric char
 *  into a space.
 */
    PRIVATE char
normalise_character (input)
    char input;         // character to be normalised.
{
    if (is_white_space (input))
        return ' ';

    return tolower (input);
}

/**
 *  Print some help info on stderr.
 */
    PRIVATE void
print_help (void)
{
    fprintf (stderr, "Invalid usage.\nThis program expects keywords to be "
      "provided as command line params, and search\ntext to be provided on "
      "stdin.\n");
}

// vim: ft=c ts=4 sw=4 et
