
TASK_PWD=/home/mps/comp90024/parallel-search/
NODES_DIR="nodes"
TIMEOUT_MINS=600

FRAGMENTS_DIR="./fragments/"
QUERY_FILE="./query"
INPUT_FILE="./data/CCCdata-small.txt"
LINES_PER_CHUNK=100000

# vim: ft=sh ts=4 sw=4 et
